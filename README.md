# SWAG Automatic Updater

## Motivation
So I currently have an SSHFS network mount on my desktop so I can access my vm-share directory remotely. On that vm-share directory, I mounted the two configuration files that I really need to update my reverse proxy. However, whenever I update those configuration files, I need to SSH into my machine and restart the container, which sort of defeats the purpose of me editing the config files remotely. Therefore, in the spirit of being lazy, I wrote two small scripts and a systemd service block that will reload the SWAG container whenever one of the scripts ("reload_swag.sh") is closed out of write-mode, using inotify. 


## Installation
```bash
# install inotify
apt install -y inotify-tools

# copy the service file into /etc/systemd/system/
cp ./update_swag.service /etc/systemd/system/

# change permissions
chmod 744 ./service_script.sh
chmod 664 /etc/systemd/system/update_swag.service

# reload systemctl daemon
systemctl daemon-reload

# enable the service
systemctl enable update_swag.service
```
