#!/bin/sh

inotifywait -mr \
	--timefmt '%d/%m/%y %H:%M' --format '%T %w %f' \
	-e close_write /home/n1mda/docker/swag/config/nginx/custom-confs |
while read -r date time dir file; do
	docker restart swag
	echo "${date} - ${time} :: ${file} was modified and swag reloaded" >&2
done
